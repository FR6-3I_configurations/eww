(defpoll net_interface :interval "1s" :initial "lo"
  "ip neigh | head --lines 1 | choose 2")

(defpoll microphone_status :interval "1s" :initial false
  "scripts/microphone/status")
(defpoll microphone_value :interval "1s" :initial 0
  "scripts/microphone/value")

(defpoll volume_status :interval "1s" :initial false
  "scripts/volume/status")
(defpoll volume_value :interval "1s" :initial 0
  "scripts/volume/value")

(defpoll time :interval "1s"
  "date '+%H:%M'")
(defpoll date :interval "1s"
  "date '+%d/%m/%Y'")

; Right side of bar
(defwidget right []
  (box :space-evenly false
    (applications)
    (disk)
    (ram)
    (cpu)
    (net_down :speed {EWW_NET[net_interface].NET_DOWN})
    (net_up :speed {EWW_NET[net_interface].NET_UP})
    (box  :space-evenly false
          :class "wheel-group"
      (microphone)
      (volume))
    (time)))

; Applications launched
(defwidget applications []
  (systray  :class "data"
            :spacing 5))

; Disk usage
(defwidget disk []
  (box  :space-evenly false
        :class "data"
        :tooltip " Root: ${round(EWW_DISK["/"].used / 1000000000, 1)} Go / ${round(EWW_DISK["/"].total / 1000000000, 1)} Go (${round(EWW_DISK["/"].used_perc, 1)} %)"
    (box :class "icon disk" " ")
    (box :width 50 "${round(EWW_DISK["/"].used_perc, 1)} %")))

; RAM usage
(defwidget ram []
  (box  :space-evenly false
        :class "data"
        :tooltip "󰘚 RAM: ${round(EWW_RAM.used_mem / 1073741824, 1)} Gio / ${round(EWW_RAM.total_mem / 1073741824, 1)} Gio (${round(EWW_RAM.used_mem_perc, 1)} %)"
    (box :class "icon ram" "󰘚 ")
    (box :width 50 "${round(EWW_RAM.used_mem_perc, 1)} %")))

; CPU usage
(defwidget cpu []
  (box  :space-evenly false
        :class "data"
    (box :class "icon cpu" "󰍛 ")
    (box :width 50 "${round(EWW_CPU.avg, 1)} %")))

; Download speed
(defwidget net_down [speed]
  (box  :space-evenly false
        :class "data"
    (box :class "icon net_down" "󰜮 ")
    (box :width 65
      {speed / 1000000 > 0.5
        ? "${round(speed / 1000000, 1)} Mo"
        : speed / 1000 > 0.5
          ? "${round(speed / 1000, 1)} Ko"
          : "${speed} o"})))

; Upload speed
(defwidget net_up [speed]
  (box  :space-evenly false
        :class "data"
    (box :class "icon net_up" "󰜷 ")
    (box :width 65
      {speed / 1000000 > 0.5
        ? "${round(speed / 1000000, 1)} Mo"
        : speed / 1000 > 0.5
          ? "${round(speed / 1000, 1)} Ko"
          : "${speed} o"})))

; Bluetooth


; Microphone
(defwidget microphone []
  (eventbox :tooltip "${microphone_status ? "󰍬" : "󰍭"} Microphone: ${microphone_value} %"
            :timeout "1s"
            :onscroll "scripts/microphone/up_or_down {}"
            :onmiddleclick "scripts/microphone/toggle_status"
            :onrightclick "alacritty -e pulsemixer & disown"
    (circular-progress  :class {!microphone_status
                                  ? "wheel"
                                  : microphone_value > 100
                                    ? "wheel high"
                                    : "wheel normal"}
                        :thickness 3
                        :start-at 35
                        :value {microphone_value > 100
                          ? (microphone_value - 100) * 0.8
                          : microphone_value * 0.8}
      (button :class {microphone_status
                        ? "icon status"
                        : "icon status disabled"}
        {microphone_status ? "󰍬" : "󰍭"}))))

; Volume
(defwidget volume []
  (eventbox :tooltip "${volume_status ? "󰓃" : "󰓄"} Volume: ${volume_value} %"
            :timeout "1s"
            :onscroll "scripts/volume/up_or_down {}"
            :onmiddleclick "scripts/volume/toggle_status"
            :onrightclick "alacritty -e pulsemixer & disown"
    (circular-progress  :class {!volume_status
                                  ? "wheel"
                                  : volume_value > 100
                                    ? "wheel high"
                                    : "wheel normal"}
                        :thickness 3
                        :start-at 35
                        :value {volume_value > 100
                          ? (volume_value - 100) * 0.8
                          : volume_value * 0.8}
      (button :class {volume_status
                        ? "icon status"
                        : "icon status disabled"}
        {volume_status ? "󰓃" : "󰓄"}))))

; Time
(defwidget time []
  (eventbox :tooltip "󰃭 Date: ${date}"
            :onclick "eww open --toggle calendar"
    (box :space-evenly false
      (box :class "icon time" "󰅐 ")
      (box :width 50 {time}))))

; Calendar
(defwindow calendar :geometry (geometry :x "81%"
                    :y "5%")
                    :stacking "fg"
  (calendar :show-week-numbers true))
