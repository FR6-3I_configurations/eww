<div align='center'>

# Eww

</div>

## 📦 Dependencies (Arch)

### ❗ Required

Official :

```shell
pacman -S \
  alacritty \
  bluez \
  bluez-utils \
  choose \
  eva \
  pulsemixer \
  wireplumber \
```

AUR :

```shell
paru -S eww leftwm
```

## ⚙️ Configuration

Use a [Nerd Font][1] to have icons. Lilex is used in this configuration (`eww.scss`).

Use it with a window manager like [this LeftWM configuration][2].

[1]: https://github.com/ryanoasis/nerd-fonts
[2]: https://gitlab.com/FR6-3I_arch/configurations/leftwm
